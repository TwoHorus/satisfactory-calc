import { expect } from "chai";
import { machines, machinesById, machinesByName } from "../../src/data/machines";
import { RECIPE_TYPES } from "../constants";

describe("data/machines", () => {
	it("should export a machines array", () => {
		expect(machines).to.be.an.instanceOf(Array);
		expect(machines.length).to.greaterThan(0);
	});

	it("should have the required fields", () => {
		for (const machine of machines) {
			expect(machine).to.have.property("id").that.is.a("number");
			expect(machine).to.have.property("name").that.is.a("string");
			expect(machine).to.have.property("icon").that.is.a("string");
			expect(machine).to.have.property("type").that.is.oneOf(RECIPE_TYPES);
			expect(machine).to.have.property("powerUsage").that.is.a("number");
			expect(machine).to.have.property("rateMultiplier").that.is.a("number");
		}
	});

	it("should not have duplicate ids", () => {
		const usedIds: number[] = [];
		for (const machine of machines) {
			expect(machine.id).to.not.be.oneOf(usedIds);
			usedIds.push(machine.id);
		}
	});

	it("should export machinesById", () => {
		for (const machine of machines) {
			expect(machinesById[machine.id]).to.equal(machine);
		}
	});

	it("should export machinesByName", () => {
		for (const machine of machines) {
			expect(machinesByName[machine.name]).to.equal(machine);
		}
	});
});

import { expect } from "chai";
import { items, itemsById, itemsByName } from "../../src/data/items";

describe("data/items", () => {
	it("should export an items array", () => {
		expect(items).to.be.an.instanceOf(Array);
		expect(items.length).to.greaterThan(0);
	});

	it("should have the required fields", () => {
		for (const item of items) {
			expect(item).to.have.property("id").that.is.a("number");
			expect(item).to.have.property("name").that.is.a("string");
			expect(item).to.have.property("icon").that.is.a("string");
			if (item.description) {
				expect(item.description).to.be.a("string");
			}
			if (item.stackSize) {
				expect(item.stackSize).to.be.a("number");
			}
		}
	});

	it("should not have duplicate ids", () => {
		const usedIds: number[] = [];
		for (const item of items) {
			expect(item.id).to.not.be.oneOf(usedIds);
			usedIds.push(item.id);
		}
	});

	it("should export itemsById", () => {
		for (const item of items) {
			expect(itemsById[item.id]).to.equal(item);
		}
	});

	it("should export itemsByName", () => {
		for (const item of items) {
			expect(itemsByName[item.name]).to.equal(item);
		}
	});
});

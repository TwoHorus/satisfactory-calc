import { expect } from "chai";
import { belts, beltsById, beltsByName } from "../../src/data/belts";

describe("data/belts", () => {
	it("should export a belts array", () => {
		expect(belts).to.be.an.instanceOf(Array);
		expect(belts.length).to.greaterThan(0);
	});

	it("should have the required fields", () => {
		for (const belt of belts) {
			expect(belt).to.have.property("id").that.is.a("number");
			expect(belt).to.have.property("name").that.is.a("string");
			expect(belt).to.have.property("rate").that.is.a("number");
			expect(belt).to.have.property("icon").that.is.a("string");
		}
	});

	it("should not have duplicate ids", () => {
		const usedIds: number[] = [];
		for (const belt of belts) {
			expect(belt.id).to.not.be.oneOf(usedIds);
			usedIds.push(belt.id);
		}
	});

	it("should export beltsById", () => {
		for (const belt of belts) {
			expect(beltsById[belt.id]).to.equal(belt);
		}
	});

	it("should export beltsByName", () => {
		for (const belt of belts) {
			expect(beltsByName[belt.name]).to.equal(belt);
		}
	});
});

import { expect } from "chai";
import * as persist from "../../src/store/persist";
import { addRequiredOutput } from "../../src/store/mutations";
import { IState, defaultState } from "../../src/store";

describe("store/mutations", () => {
	let state: IState;

	beforeEach("Setup State", () => {
		// Reset the state to a fresh copy before each test
		state = defaultState();
	});

	describe("dehydrate", () => {
		it("should be deterministic", () => {
			const str1 = persist.dehydrate(state);
			const str2 = persist.dehydrate(state);
			expect(str1).to.equal(str2);
		});
	});

	describe("hydrate", () => {
		it("should return the same value we passed to dehydrate", () => {
			const str = persist.dehydrate(state);
			const newState = persist.hydrate(str);
			expect(newState).to.deep.equal(state);
		});
	});

	describe("clearSavedState", () => {
		it("should remove any items created by saveState", () => {
			const storedItems = window.localStorage.length;
			persist.saveState(state);
			expect(window.localStorage.length).to.not.equal(storedItems);
			persist.clearSavedState();
			expect(window.localStorage.length).to.equal(storedItems);
		});
	});

	describe("saveState", () => {
		afterEach(() => {
			persist.clearSavedState();
		});

		it("should store something in local storage", () => {
			const storedItems = window.localStorage.length;
			persist.saveState(state);
			expect(window.localStorage.length).to.not.equal(storedItems);
		});
	});

	describe("loadState", () => {
		afterEach(() => {
			persist.clearSavedState();
		});

		it("should load the same state", () => {
			addRequiredOutput(state, { itemId: 1, rate: 60 });
			const outputs = state.requiredOutputs.length;
			persist.saveState(state);

			// Reset the state
			state = defaultState();

			expect(state.requiredOutputs.length).to.not.equal(outputs);

			state = persist.loadState();
			expect(state.requiredOutputs.length).to.equal(outputs);
		});

		it("should load the default state if none is present", () => {
			persist.clearSavedState();
			const newState = persist.loadState();
			expect(newState).to.deep.equal(defaultState());
		});

		it("should load the default state if the version doesn't match", () => {
			addRequiredOutput(state, { itemId: 1, rate: 60 });
			const outputs = state.requiredOutputs.length;

			// Override the saved version id
			window.localStorage.setItem(persist.STATE_VERSION_KEY, "INVALID VERSION");

			const newState = persist.loadState();

			expect(newState.requiredOutputs.length).to.not.equal(outputs);
		});
	});
});

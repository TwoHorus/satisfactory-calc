jsdomGlobal = require("jsdom-global");

jsdomGlobal(undefined, {
	url: "https://nulifier.gitlab.io/satisfactory-calc/"
});

// Workaround until this issue is fixed:
// https://github.com/vuejs/vue/issues/9698
global.performance = window.performance;

import { keyBy } from "lodash";

import { IItem } from "../models";

export const items = [
	{ id: 1, name: "Iron Ore", stackSize: 100, description: "", icon: "images/icons/iron-ore.png" },
	{ id: 2, name: "Copper Ore", stackSize: 100, description: "", icon: "images/icons/copper-ore.png" },
	{ id: 3, name: "Limestone", stackSize: 100, description: "", icon: "images/icons/limestone.png" },
	{ id: 4, name: "Coal", stackSize: 0, description: "", icon: "images/icons/coal.png" },
	{ id: 5, name: "Crude Oil", stackSize: 100, description: "Crude oil is refined into all kinds of Oil-based resources, like Fuel, Rubber and Plastic.", icon: "images/icons/crude-oil.png" },
	{ id: 6, name: "Gold Ore", stackSize: 0, description: "", icon: "" },
	{ id: 7, name: "Caterium Ore", stackSize: 0, description: "", icon: "images/icons/caterium-ore.png" },
	{ id: 8, name: "Raw Quartz", stackSize: 0, description: "", icon: "images/icons/raw-quartz.png" },
	{ id: 9, name: "Sulfur", stackSize: 0, description: "", icon: "images/icons/sulfur.png" },
	{ id: 10, name: "Bauxite", stackSize: 0, description: "", icon: "images/icons/bauxite.png" },
	{ id: 11, name: "S.A.M. Ore", stackSize: 50, description: "Strange Alien Metal that glistens with new possibilities.", icon: "images/icons/sam-ore.png" },
	{ id: 12, name: "Silica", stackSize: 0, description: "", icon: "" },
	{ id: 13, name: "Uranium", stackSize: 0, description: "", icon: "images/icons/uranium.png" },
	{ id: 14, name: "Alien Carapace", stackSize: 0, description: "", icon: "images/icons/alien-carapace.png" },
	{ id: 15, name: "Alien Organs", stackSize: 0, description: "", icon: "images/icons/alien-organs.png" },
	{ id: 16, name: "Leaves", stackSize: 0, description: "", icon: "images/icons/leaves.png" },
	{ id: 17, name: "Wood", stackSize: 0, description: "", icon: "images/icons/wood.png" },
	{ id: 18, name: "Vines", stackSize: 0, description: "", icon: "images/icons/vines.png" },
	{ id: 19, name: "Mycelia", stackSize: 0, description: "", icon: "images/icons/mycelia.png" },
	{ id: 20, name: "Flower Petals", stackSize: 0, description: "", icon: "images/icons/flower-petals.png" },
	{ id: 21, name: "Green Power Slug", stackSize: 0, description: "The crystals on these slugs can be harvested and converted into power shards that function with current FICSIT technology.", icon: "images/icons/green-power-slug.png" },
	{ id: 22, name: "Yellow Power Slug", stackSize: 0, description: "The crystals on these slugs can be harvested and converted into power shards that function with current FICSIT technology.", icon: "images/icons/yellow-power-slug.png" },
	{ id: 23, name: "Purple Power Slug", stackSize: 0, description: "The crystals on these slugs can be harvested and converted into power shards that function with current FICSIT technology.", icon: "images/icons/purple-power-slug.png" },
	{ id: 24, name: "Beryl Nut", stackSize: 100, description: "Can be eaten to restore half a health segment.", icon: "images/icons/beryl-nut.png" },
	{ id: 25, name: "Paleberry", stackSize: 50, description: "Can be eaten to restore one health segment.", icon: "images/icons/paleberry.png" },
	{ id: 26, name: "Bacon Agaric", stackSize: 50, description: "Can be eaten to restore two health segments.", icon: "images/icons/bacon-agaric.png" },
	{ id: 27, name: "Somersloop", stackSize: 0, description: "A strange alien thing with a mind-being and somehow familiar shape.", icon: "images/icons/somersloop.png" },
	{ id: 28, name: "Mercer Sphere", stackSize: 0, description: "A Weird alien thing with an impossibly smooth surface.", icon: "images/icons/mercer-sphere.png" },
	{ id: 29, name: "Hard Drive", stackSize: 0, description: "A hard drive with Ficsit data. Analyze it in the M.A.M. to salvage its contents.", icon: "images/icons/hard-drive.png" },
	{ id: 30, name: "Iron Ingot", stackSize: 0, description: "Used for crafting. Crafted into the most basic parts.", icon: "images/icons/iron-ingot.png" },
	{ id: 31, name: "Iron Plate", stackSize: 0, description: "Used for crafting. One of the most basic parts.", icon: "images/icons/iron-plate.png" },
	{ id: 32, name: "Iron Rod", stackSize: 0, description: "Used for crafting. One of the most basic parts.", icon: "images/icons/iron-rod.png" },
	{ id: 33, name: "Copper Ingot", stackSize: 0, description: "", icon: "images/icons/copper-ingot.png" },
	{ id: 34, name: "Wire", stackSize: 500, description: "", icon: "images/icons/wire.png" },
	{ id: 35, name: "Cable", stackSize: 0, description: "Used for crafting and connecting Buildings.", icon: "images/icons/cable.png" },
	{ id: 36, name: "Biomass", stackSize: 0, description: "Primarily used as fuel. Biomass burners and vehicles can use it for power. Compressed biological matter.", icon: "images/icons/biomass.png" },
	{ id: 37, name: "Caterium Ingot", stackSize: 0, description: "", icon: "images/icons/caterium-ingot.png" },
	{ id: 38, name: "Quickwire", stackSize: 0, description: "", icon: "images/icons/quickwire.png" },
	{ id: 39, name: "Concrete", stackSize: 0, description: "", icon: "images/icons/concrete.png" },
	{ id: 40, name: "Screw", stackSize: 500, description: "Used for crafting. One of the most basic parts.", icon: "images/icons/screw.png" },
	{ id: 41, name: "Reinforced Iron Plate", stackSize: 0, description: "", icon: "images/icons/reinforced-iron-plate.png" },
	{ id: 42, name: "Biofuel", stackSize: 0, description: "", icon: "images/icons/biofuel.png" },
	{ id: 43, name: "Rotor", stackSize: 0, description: "Used for crafting. The moving parts of a motor.", icon: "images/icons/rotor.png" },
	{ id: 44, name: "Modular Frame", stackSize: 0, description: "Used for crafting. Multi-purpose building block.", icon: "images/icons/modular-frame.png" },
	{ id: 45, name: "Color Cartridge", stackSize: 100, description: "Necessary to change the color of factory buildings and vehicles.", icon: "images/icons/color-cartridge.png" },
	{ id: 46, name: "Power Shard", stackSize: 100, description: "Mucus from the power slugs compressed into a solid crystal-like shard. It radiates a strange power.", icon: "images/icons/power-shard.png" },
	{ id: 47, name: "Fabric", stackSize: 0, description: "", icon: "images/icons/fabric.png" },
	{ id: 48, name: "Spiked Rebar", stackSize: 50, description: "Ammo for the Rebar Gun", icon: "images/icons/spiked-rebar.png" },
	{ id: 49, name: "High-Speed Connector", stackSize: 0, description: "", icon: "images/icons/high-speed-connector.png" },
	{ id: 50, name: "Steel Ingot", stackSize: 0, description: "Steel Ingots are made from Iron Ore that's been smelted with Coal. They are made into several parts used in building construction.", icon: "images/icons/steel-ingot.png" },
	{ id: 51, name: "Steel Beam", stackSize: 100, description: "Steel Beams are used most often when constructing a little more advanced buildings.", icon: "images/icons/steel-beam.png" },
	{ id: 52, name: "Steel Pipe", stackSize: 0, description: "Steel Pipes are used most often when constructing a little more advanced buildings.", icon: "images/icons/steel-pipe.png" },
	{ id: 53, name: "Encased Industrial Beam", stackSize: 100, description: "Encased Industrial Beams utilizes the compressive strength of concrete and tensile strength of steel simultaneously. Mostly used as a stable basis for constructing buildings", icon: "images/icons/encased-industrial-beam.png" },
	{ id: 54, name: "Stator", stackSize: 0, description: "The static parts of a motor.", icon: "images/icons/stator.png" },
	{ id: 55, name: "Motor", stackSize: 0, description: "The Motor creates a mechanical force that is used to move things from machines to vehicles.", icon: "images/icons/motor.png" },
	{ id: 56, name: "Heavy Modular Frame", stackSize: 50, description: "A more robust multi-purpose frame.", icon: "images/icons/heavy-modular-frame.png" },
	{ id: 57, name: "Plastic", stackSize: 0, description: "A versatile and easy to manufacture material that can be used for a lot of things.", icon: "images/icons/plastic.png" },
	{ id: 58, name: "Fuel", stackSize: 0, description: "", icon: "images/icons/fuel.png" },
	{ id: 59, name: "Rubber", stackSize: 0, description: "", icon: "images/icons/rubber.png" },
	{ id: 60, name: "Circuit Board", stackSize: 0, description: "", icon: "images/icons/circuit-board.png" },
	{ id: 61, name: "Computer", stackSize: 50, description: "A computer is a complex logic machine that is used to control advanced behavior in machines.", icon: "images/icons/computer.png" },
	{ id: 62, name: "Nuclear Fuel Rod", stackSize: 0, description: "", icon: "" },
	{ id: 63, name: "Aluminum Ingot", stackSize: 0, description: "", icon: "" },
	{ id: 64, name: "Aluminum Sheet", stackSize: 0, description: "", icon: "" },
	{ id: 65, name: "Heat Sink", stackSize: 0, description: "", icon: "" },
	{ id: 66, name: "Radio Control Unit", stackSize: 0, description: "", icon: "" },
	{ id: 67, name: "Turbo Motor", stackSize: 0, description: "", icon: "" },
	{ id: 68, name: "Battery", stackSize: 0, description: "Batteries store an electrical charge. They are primarily used as a power source.", icon: "images/icons/battery.png" },
	{ id: 69, name: "Supercomputer", stackSize: 0, description: "", icon: "images/icons/supercomputer.png" },
	{ id: 70, name: "Quantum Crystal", stackSize: 0, description: "", icon: "" },
	{ id: 71, name: "Superpostion Oscillator", stackSize: 0, description: "", icon: "" },
	{ id: 72, name: "Quantum Computer", stackSize: 0, description: "", icon: "" },
	{ id: 73, name: "Portable Miner", stackSize: 1, description: "Can be set up on a resource node to automatically extract the resource. Very limited storage space.", icon: "images/icons/portable-miner.png" },
	{ id: 74, name: "Xeno-Zapper", stackSize: 1, description: "Standard issue electroshock self defense weapon for melee range.", icon: "images/icons/xeno-zapper.png" },
	{ id: 75, name: "Chainsaw", stackSize: 1, description: "Used to clear an area of flora that is too difficult to remove by hand.", icon: "images/icons/chainsaw.png" },
	{ id: 76, name: "Object Scanner", stackSize: 1, description: "Scans the immediate area for a set item. Beeps at a rate proportional to proximity.", icon: "images/icons/object-scanner.png" },
	{ id: 77, name: "Beacon", stackSize: 100, description: "Used to mark areas of interest. Displayed on your compass with the color and name you set for it.", icon: "images/icons/beacon.png" },
	{ id: 78, name: "Color Gun", stackSize: 1, description: "Paints factory buildings and vehicles. The color can be adjusted prior to building.", icon: "images/icons/color-gun.png" },
	{ id: 79, name: "Parachute", stackSize: 50, description: "Slows down your fall when activated in mid-air.", icon: "images/icons/parachute.png" },
	{ id: 80, name: "Rebar Gun", stackSize: 1, description: "Improvised ranged weapon for self defense. Has to be reloaded after each use.", icon: "images/icons/rebar-gun.png" },
	{ id: 81, name: "Xeno-Basher", stackSize: 1, description: "Heavy electroshock self defense weapon for melee range.", icon: "images/icons/xeno-basher.png" },
	{ id: 82, name: "Medical Inhaler", stackSize: 50, description: "Can be inhaled to fully restore health.", icon: "images/icons/medical-inhaler.png" },
	{ id: 83, name: "Jetpack", stackSize: 0, description: "", icon: "images/icons/jetpack.png" },
	{ id: 84, name: "Gasmask", stackSize: 0, description: "", icon: "images/icons/gasmask.png" },
	{ id: 85, name: "Filter", stackSize: 0, description: "", icon: "images/icons/filter.png" },
	{ id: 86, name: "Heat Resistant Suit", stackSize: 0, description: "", icon: "" },
	{ id: 87, name: "Radioactivity Resistant Suit", stackSize: 0, description: "", icon: "" },
	{ id: 88, name: "A.I. Limiter", stackSize: 0, description: "A.I. Limiters are super advanced electronics that are used to control A.I.s and keep them from evolving in malicious ways.", icon: "images/icons/ai-limiter.png" }
] as IItem[];

export const itemsById = keyBy(items, (item) => item.id);
export const itemsByName = keyBy(items, (item) => item.name);

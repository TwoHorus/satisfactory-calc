import { graphlib } from "dagre-d3";
import { round } from "../../util";
import { IMachine, IItem } from "../../models";

let nextId = 0;

export function generateId(type: string) {
	return `${type}-${nextId++}`;
}

export function createMachineNode(g: graphlib.Graph, machine: IMachine, output: IItem, quantity?: number) {
	const key = generateId(machine.name);
	const labelType = "html";
	const quantityLabel = quantity ? `<span>${round(quantity, 1)}x</span>` : "";
	const labelDom =
		`<div class="graph-machine" style="width: 180px;">` +
			`<div class="machine-icon">` +
				quantityLabel +
				`<img width="32" height="32" src="${output.icon}" />` +
			`</div>` +
			`<div class="machine-name">${machine.name}</div>` +
			`<div class="recipe-name">${output.name}</div>` +
		`</div>`;
	g.setNode(key, { labelType, label: labelDom });
	return key;
}

// tslint:disable-next-line:interface-name
declare interface Dictionary<T> {
	[key: string]: T;
}

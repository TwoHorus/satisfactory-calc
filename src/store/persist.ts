import { Store } from "vuex";
import { IState, defaultState } from ".";

export const STORE_VERSION = "5";
export const STATE_VERSION_KEY = "CALC_STATE_VERSION";
export const STATE_KEY = "CALC_STATE";

export function dehydrate(state: IState): string {
	return JSON.stringify(state);
}

export function hydrate(str: string): IState {
	return JSON.parse(str);
}

export function loadState(): IState {
	const version = window.localStorage.getItem(STATE_VERSION_KEY);
	const stateStr = window.localStorage.getItem(STATE_KEY);

	// If the version matches, load the state from storage, if it doesn't load the default state
	if (version === STORE_VERSION && stateStr) {
		const state = hydrate(stateStr);
		// We may have stored that we don't want to persist the state, check that
		if (state.persistState) {
			return hydrate(stateStr);
		}
		else {
			// Return the default state with the persist flag set to false
			return defaultState(false);
		}
	}
	else {
		return defaultState();
	}
}

export function saveState(state: IState) {
	window.localStorage.setItem(STATE_VERSION_KEY, STORE_VERSION);
	window.localStorage.setItem(STATE_KEY, dehydrate(state));
}

export function clearSavedState() {
	window.localStorage.removeItem(STATE_VERSION_KEY);
	window.localStorage.removeItem(STATE_KEY);
}

export default function persist(store: Store<IState>) {
	store.subscribe((mutation, state) => {
		// Check if this mutation is turning persistence off
		const persistOff = (mutation.type === "enablePersistence") && (mutation.payload === false);

		// We only want to persist the state if its selected or if this mutation is turning off persistence
		if (state.persistState || persistOff) {
			saveState(state);
		}
	});
}

import Vue from "vue";
import Vuex from "vuex";
import { IItemRate, NodePurity } from "../models";
import * as mutations from "./mutations";
import * as getters from "./getters";
import { itemsByName } from "../data/items";
import persistPlugin, { loadState } from "./persist";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export interface IState {
	persistState: boolean;
	requiredOutputs: IItemRate[];
	disabledBelts: number[];
	disabledMachines: number[];
	enabledRecipes: number[];
	nodePurity: NodePurity;
}

export function defaultState(persist = true) {
	return {
		persistState: persist,
		requiredOutputs: [{ itemId: itemsByName["Reinforced Iron Plate"].id, rate: 60 }],
		disabledBelts: [],
		disabledMachines: [],
		enabledRecipes: [],
		nodePurity: "Normal"
	} as IState;
}

const store = new Vuex.Store<IState>({
	state: loadState(),
	mutations,
	getters,
	plugins: [persistPlugin],
	strict: debug
});

export default store;

if (module.hot) {
	module.hot.accept(["./mutations", "./getters"], () => {
		// Require the updated modules
		const newMutations = require("./mutations").default;
		const newGetters = require("./getters").default;

		// Swap in the new modules and mutations
		store.hotUpdate({
			mutations: newMutations,
			getters: newGetters
		});
	});
}
